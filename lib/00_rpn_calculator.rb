require 'byebug'

class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def tokens(str)
    str.split.map do |el|
      "1234567890".include?(el) ? el.to_i : el.to_sym
    end
  end

  def evaluate(str)
    tokens = tokens(str)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def perform_operation(operator)
    raise "calculator is empty" if @stack.size < 2
    first_num = @stack.pop
    second_num = @stack.pop

    case operator
    when :+
      @stack << second_num + first_num
    when :-
      @stack << second_num - first_num
    when :*
      @stack << second_num * first_num
    when :/
      @stack << second_num / first_num.to_f
    else
      @stack << second_num
      @stack << first_num
      raise "No such operation #{operator}"
    end
  end
end
